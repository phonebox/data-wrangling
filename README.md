# Data-wrangling toolkit for small organisations

This repository is a work in progress for tools Phonebox Productions uses to help small community organisations and not-for-profits get to grips with the unique challenges of dealing with organisational data with limited resources and tech skills. Feel free to use and adapt these and attribute and share alike under Creative Commons CC-BY-SA 4.0 license.



