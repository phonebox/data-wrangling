---
Author: Sophea Lerner
License: CC BY-NC-SA 4.0
---

# Data-wrangling workshop : Preparation

## Preparing for your session

**Duration**: A few minutes.

**When**: 2-3 days before we meet.

**What**: Take a few minutes to scan through the list of prompts. For each one just notice briefly how you feel about this aspect of you current workflow. There are no right answers.

Your response might be anything from: "I haven't thought about that", "I don't see how that is relevant to what we do" or "I don't know what that is." through to more emotional reactions such as overwhelm, or satisfaction with how that aspect is running, or more practical observations such as what parts of your process relate to that question or just noticing where you might need more analysis to figure that out.

You don't need to write anything down unless you are strongly compelled to write a specific note to remind yourself of an issue or question for your own benefit. If you can read them once, sleep on them a couple of times and let them be in the back of your mind during a couple of normal working days then that's great. There will be an opportunity to revisit these areas in more detail later.

**Why**: You probably would not have called us to look at your data-wrangling work-flow if things were going completely smoothly. Right now you are probably snowed under and do not have time to devote to extensive preparation for our session. This exercise is designed to give some jobs to the back of you mind while you continue to deal with other things on your plate. It may seem a bit unusual, but we have developed this tool as a way to fast track building a shared language about your organisation and it's processes; and help relevant issues come to your attention in the days before our workshop. Unlike a formal task analysis, it does not require any specialised tech skills, and takes very little time. Just drop these questions in your brain, notice your immediate thoughts and reactions, and then sleep on it till we meet. This is intended to help us begin to establish a baseline for identifying areas to address and prioritising them. Whilst tech may be in focus here, really this is just about how your organisation gets things done.

***When you have a few minutes without interruptions, make yourself a warm beverage and proceed to the next section. You can do this quietly on your own, or together if you prefer.***

____

## Preparatory exercise

***Scan through the following prompts and make a quick mental note of your immediate response. You don't have to answer the questions now, just ask them. Just let a picture form in your mind of how the question relates to where your organisation is at is now, where you see it going and how you feel that is working. If something seems like a problem that's okay, you don't have to solve it, just notice it. Some questions may seem more important to you than others. Don't spend long on each point.***

What kinds of data come into your organisation?

How do you store them? For how long?

How do you use these data? What jobs do these data have within your organisation?

Who has access to different data? where do they need to access it?

How do you share data at the moment?

What would you like your data to do that it currently cannot? e.g. analytics/stats

What factors impact how you handle data? Regulatory environment? Privacy issues? Transparency requirements?

How are different data affected by these concerns?

Which data do you currently need to keep private? How is it secured?

What steps do you have in place to prevent a) data loss b) data breach?

Would you know if your data had been breached? Who would be affected?

How does your organisation store passwords right now?

If your computer hard drive failed tomorrow how would it affect your work?

Do you keep archives? How accessible do these need to be? for how long?

Do you need to have multiple versions of some documents? How do you keep track of them?

How much time do you spend finding digital files?

What data goes out of your organisation? to where? in what formats?
How do you keep track of it? update it?

Is your organisation likely to hire more staff in the future? Imagine it's your job to explain to them how everything works. What would make that job easier?

If you were to suddenly and completely hypothetically disappear would someone else be able to step into your role? What sort of documentation might you need of processes that you currently maintain?

*

What kinds of tech problems do you regularly encounter?

How do you solve them? Do you have a troubleshooting process? Is there someone you can ask for help?

How often do technical issues slow down your work?

Are there activities you find yourself repeating over and over such as copying and pasting data between documents or converting data from one format to another or sequences of mouse clicks that you do again and again?

What are the tools and processes you currently use which you are very invested in for any reason(e.g. because of a contract or because you have spent a long time learning to use it really well)?

What are your biggest 'expense' items in your current toolkit (either in terms of money spent or time invested in working with a tool or system.)?

Which aspects of your tech set-up currently cause you the greatest frustration?

Which aspects of your current day to day process give you the greatest feeling of satisfaction and mastery with the tools you are using?

Are there questions that are not asked here that you have asked yourself while thinking about these?

What's on your tech/data wish-list?

_**That's it. Job done. Thanks for taking a few minutes to check-in with the your data landscape and work processes. See you in a couple of days!**_

***Your confidential data:*** _Some of these questions pertain to operationally sensitive matters, any discussion regarding these issues will be in strictest confidence. We will never ask you to respond to these kinds of questions over e.g. unsecured email. Our notes from these meetings will be stored on encrypted media that no-one else has access to, and kept only for as long as they are relevant to our work together._
